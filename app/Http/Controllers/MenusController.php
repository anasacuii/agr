<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();

        return view('menus.index', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu;
        $menu->name = $request["name"];
        $menu->price = $request["price"];
        $menu->description = $request["description"];
        $menu->type = $request["type"];
        $menu->save();

        return redirect('/display_menus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return view('menus.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function display_deserts_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_deserts', compact('menus'));
    }
     public function display_nonalcols_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_nonalcols', compact('menus'));
    }
     public function display_pizzas_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_pizzas', compact('menus'));
    }
     public function display_specials_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_specials', compact('menus'));
    }
    public function display_alcols_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_alcols', compact('menus'));
    }
    public function display_lunchs_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_lunchs', compact('menus'));
    }
    
    public function display_breakfasts_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_breakfasts', compact('menus'));
    }
    public function display_snacks_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_snacks', compact('menus'));
    }
     public function display_ciorbes_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_ciorbes', compact('menus'));
    }
     public function display_principals_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_principals', compact('menus'));
    }
     public function display_pastes_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_pastes', compact('menus'));
    }
     public function display_salads_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_salads', compact('menus'));
    }
     public function display_garnitures_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_garnitures', compact('menus'));
    }
     public function display_vegans_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_vegans', compact('menus'));
    }
     public function display_glutens_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_glutens', compact('menus'));
    }
     public function display_raws_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_raws', compact('menus'));
    }
     public function display_diabetics_menus()
    {
        //
        $menus = Menu::all();

        return view('menus.display_diabetics', compact('menus'));
    }
}

?>