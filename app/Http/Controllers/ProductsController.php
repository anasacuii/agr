<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->name = $request["name"];
        $product->quantity = $request["quantity"];
        $product->supply_date = $request["supply_date"];
        $product->type = $request["type"];
        $product->group = $request["group"];
        $product->save();

        return redirect('/display_products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //Product types
    public function display_fruits()
    {
        $products = Product::all();

        return view('products.display_fruits', compact('products'));
    }

    public function display_vegetables()
    {
        $products = Product::all();

        return view('products.display_vegetables', compact('products'));
    }

    public function display_spices()
    {
        $products = Product::all();

        return view('products.display_spices', compact('products'));
    }

    public function display_grains()
    {
        $products = Product::all();

        return view('products.display_grains', compact('products'));
    }

    public function display_alcoholic()
    {
        $products = Product::all();

        return view('products.display_alcoholic', compact('products'));
    }

    public function display_nonalcoholic()
    {
        $products = Product::all();

        return view('products.display_nonalcoholic', compact('products'));
    }

    public function display_crockery()
    {
        $products = Product::all();

        return view('products.display_crockery', compact('products'));
    }

    //Product groups
    public function display_glutenfree()
    {
        $products = Product::all();

        return view('product_groups.display_glutenfree', compact('products'));
    }

    public function display_diabetics()
    {
        $products = Product::all();

        return view('product_groups.display_diabetics', compact('products'));
    }

    public function display_vegan()
    {
        $products = Product::all();

        return view('product_groups.display_vegan', compact('products'));
    }

    public function display_rawvegan()
    {
        $products = Product::all();

        return view('product_groups.display_rawvegan', compact('products'));
    }




}
