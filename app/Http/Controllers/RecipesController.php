<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Recipe;

class RecipesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $recipes = Recipe::all();

        return view('recipes.index', compact('recipes')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('recipes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $recipe = new Recipe;
        $recipe->name = $request["name"];
        $recipe->type = $request["type"];
        $recipe->description = $request["description"];
        $recipe->save();

        return redirect('/display_recipes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function display_breakfast()
    {
      $recipes = Recipe::all();

      return view('recipes.display_breakfast', compact('recipes'));   
    }

    public function display_lunch()
    {
      $recipes = Recipe::all();

      return view('recipes.display_lunch', compact('recipes'));   
    }

    public function display_desert()
    {
      $recipes = Recipe::all();

      return view('recipes.display_desert', compact('recipes'));   
    }

    public function display_cocktail()
    {
      $recipes = Recipe::all();

      return view('recipes.display_cocktail', compact('recipes'));   
    }
}
