<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/**
 * Everything in this group can only be accessed by a logged in user.
 * Trying to access this routes while logged out will redirect you to the
 * login page.
 *
 * @DOC - https://laravel.com/docs/5.1/middleware
 */
Route::group(['middleware' => 'auth'], function () {
    // Products routes
    Route::get('display_products', 'ProductsController@index');
    Route::get('display_fruits', 'ProductsController@display_fruits');
    Route::get('display_vegetables', 'ProductsController@display_vegetables');
    Route::get('display_spices', 'ProductsController@display_spices');
    Route::get('display_grains', 'ProductsController@display_grains');
    Route::get('display_alcoholic', 'ProductsController@display_alcoholic');
    Route::get('display_nonalcoholic', 'ProductsController@display_nonalcoholic');
    Route::get('display_crockery', 'ProductsController@display_crockery');
    Route::get('create_product', ['middleware' => ['role:manager|bucatar'], 'uses' => 'ProductsController@create']);
    Route::post('save_product', 'ProductsController@store');

    //Product Groups
    Route::get('display_glutenfree', 'ProductsController@display_glutenfree');
    Route::get('display_diabetics', 'ProductsController@display_diabetics');
    Route::get('display_vegan', 'ProductsController@display_vegan');
    Route::get('display_rawvegan', 'ProductsController@display_rawvegan');

    // Recipe routes
    Route::get('display_recipes', 'RecipesController@index');
    Route::get('display_breakfast', 'RecipesController@display_breakfast');
    Route::get('display_lunch', 'RecipesController@display_lunch');
    Route::get('display_desert', 'RecipesController@display_desert');
    Route::get('display_cocktail', 'RecipesController@display_cocktail');
    Route::get('create_recipe', 'RecipesController@create');
    Route::post('save_recipe', 'RecipesController@store');

    // Suppliers routes
    Route::get('display_suppliers', 'SuppliersController@index');
    Route::get('display_products_suppliers', 'SuppliersController@display_products_suppliers');
    Route::get('display_equipment_suppliers', 'SuppliersController@display_equipment_suppliers');
    Route::get('create_supplier', ['middleware' => ['role:manager'], 'uses' => 'SuppliersController@create']);
    Route::post('save_supplier', 'SuppliersController@store');

    //Reservations routes
    Route::get('display_reservations', 'ReservationsController@index');
    Route::post('save_reservation', 'ReservationsController@store');
    Route::get('create_reservation', ['middleware' => ['role:manager|ospatar'], 'uses' => 'ReservationsController@create']);

    //Users routes
    Route::get('display_users', 'UsersController@index');

    //Orders routes
    Route::get('display_orders', 'OrdersController@index');
    Route::post('save_order', 'OrdersController@store');
    Route::get('create_order', ['middleware' => ['role:manager|ospatar'], 'uses' => 'OrdersController@create']);

    //Menu routes
    Route::get('display_menus', 'MenusController@index');
    Route::get('display_deserts_menus', 'MenusController@display_deserts_menus');
    Route::get('display_lunchs_menus', 'MenusController@display_lunchs_menus');
    Route::get('display_ciorbes_menus', 'MenusController@display_ciorbes_menus');
    Route::get('display_pastes_menus', 'MenusController@display_pastes_menus');
    Route::get('display_salads_menus', 'MenusController@display_salads_menus');
    Route::get('display_garnitures_menus', 'MenusController@display_garnitures_menus');
    Route::get('display_principals_menus', 'MenusController@display_principals_menus');
    Route::get('display_pizzas_menus', 'MenusController@display_pizzas_menus');
    Route::get('display_specials_menus', 'MenusController@display_specials_menus');
    Route::get('display_vegans_menus', 'MenusController@display_vegans_menus');
    Route::get('display_glutens_menus', 'MenusController@display_glutens_menus');
    Route::get('display_raws_menus', 'MenusController@display_raws_menus');
    Route::get('display_diabetics_menus', 'MenusController@display_diabetics_menus');
    Route::get('display_alcols_menus', 'MenusController@display_alcols_menus');
    Route::get('display_nonalcols_menus', 'MenusController@display_nonalcols_menus');
    Route::get('display_breakfasts_menus', 'MenusController@display_breakfasts_menus');
    Route::get('display_snacks_menus', 'MenusController@display_snacks_menus');
    Route::get('create_menu', ['middleware' => ['role:manager'], 'uses' => 'MenusController@create']);
    Route::post('save_menu', 'MenusController@store');
});

