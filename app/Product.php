<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
     use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'quantity', 'type', 'group'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'supply_date'];

    public function menus()
    {
        return $this->belongsToMany('App\Menu', 'product_menu', 'product_id',
            'menu_id');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Recipe', 'product_recipe',
            'product_id', 'recipe_id');
    }
}

