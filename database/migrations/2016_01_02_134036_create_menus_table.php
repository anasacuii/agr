<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('price', 15, 2);
            $table->longText('description')->nullable();
            $table->enum('type', ['alcol', 'dejun', 'ciorba', 'desert', 'diabetic', 'garnituri', 'gluten', 'nonalcol', 'paste', 'pizza', 'principal', 'raw', 'salata', 'snack', 'vegan']);
            /*$table->enum('type', ['alcol', 'dejun', 'spices', 'grains', 'alcoholic', 'nonalcoholic', 'crockery']);*/

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}