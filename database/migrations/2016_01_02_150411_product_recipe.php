<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductRecipe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for associating roles to users (Many-to-Many)
        Schema::create('product_recipe', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('recipe_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('recipe_id')->references('id')->on('recipes');

            $table->primary(['product_id', 'recipe_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_recipe');
    }
}
