<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductProductGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table for associating roles to users (Many-to-Many)
        Schema::create('product_product_group', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('product_group_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('product_group_id')->references('id')->on('product_groups');

            $table->primary(['product_id', 'product_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_product_group');
    }
}
