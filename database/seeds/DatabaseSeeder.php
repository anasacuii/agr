<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(EntrustRoleAndPermissionsTableSeeder::class);

        Model::reguard();
    }
}

class EntrustRoleAndPermissionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $manager = App\Role::create(['name' => 'manager', 'display_name' => 'Manager', 'description' => 'User manager rights']);
        $ospatar = App\Role::create(['name' => 'ospatar', 'display_name' => 'Ospatar', 'description' => '']);
        $bucatar = App\Role::create(['name' => 'bucatar', 'display_name' => 'Bucatar', 'description' => '']);
        $barman = App\Role::create(['name' => 'barman', 'display_name' => 'Barman', 'description' => '']);

    }

}
