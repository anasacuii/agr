<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Agr</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">Agr</a>
			</div>

			<div class="collapse navbar-collapse" id="navbar">
                <!-- Aici se aduga linkuri noi in bara de navigatii -->
                @if(!auth()->guest())
    				<ul class="nav navbar-nav">
    				@if(auth()->user()->hasRole(['manager', 'ospatar', 'bucatar', 'barman']))
    			    <li><a href="{{ url('/display_menus') }}">Meniu</a></li>
                	@endif
                	@if(auth()->user()->hasRole(['manager', 'ospatar', 'bucatar', 'barman']))
    			    <li><a href="{{ url('/display_orders') }}">Comenzi</a></li>
                	@endif
                	@if(auth()->user()->hasRole(['manager']))
    			    <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Furnizori<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/display_products_suppliers') }}">Furnizori de produse</a></li>
								<li><a href="{{ url('/display_equipment_suppliers') }}">Furnizori de echipamente</a></li>
								<li><a href="{{ url('/display_suppliers') }}">Toti</a></li>
							</ul>
						</li>
                	@endif
    				@if(auth()->user()->hasRole(['manager','bucatar','barman']))
    			    <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Produse<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/display_fruits') }}">Fructe</a></li>
								<li><a href="{{ url('/display_vegetables') }}">Legume</a></li>
								<li><a href="{{ url('/display_spices') }}">Condimente</a></li>
								<li><a href="{{ url('/display_grains') }}">Fainoase</a></li>
								<li><a href="{{ url('/display_alcoholic') }}">Bauturi alcoolice</a></li>
								<li><a href="{{ url('/display_nonalcoholic') }}">Bauturi nonalcoolice</a></li>
								<li><a href="{{ url('/display_crockery') }}">Vesela</a></li>
								<li><a href="{{ url('/display_products') }}">Toate</a></li>
							</ul>
						</li>
                	@endif	
    				@if(auth()->user()->hasRole(['manager','bucatar']))
    			    <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Grupe de Produse<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/display_glutenfree') }}">Fara gluten</a></li>
								<li><a href="{{ url('/display_diabetics') }}">Pentru diabetici</a></li>
								<li><a href="{{ url('/display_vegan') }}">Vegan</a></li>
								<li><a href="{{ url('/display_rawvegan') }}">Raw-vegan</a></li>
								
							</ul>
						</li>
                	@endif	
    				@if(auth()->user()->hasRole(['manager','bucatar']))
    			    <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Retete<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/display_breakfast') }}">Mic dejun</a></li>
								<li><a href="{{ url('/display_lunch') }}">Pranz/Cina</a></li>
								<li><a href="{{ url('/display_desert') }}">Desert</a></li>
								<li><a href="{{ url('/display_cocktail') }}">Cocktail</a></li>
								<li><a href="{{ url('/display_recipes') }}">Toate</a></li>
								
							</ul>
						</li>
                	@endif
                	@if(auth()->user()->hasRole(['manager', 'ospatar']))
    			    <li><a href="{{ url('/display_reservations') }}">Rezervari</a></li>
                	@endif
                	@if(auth()->user()->hasRole(['manager']))
    			    <li><a href="{{ url('/display_users') }}">Utilizatori</a></li>
                	@endif
    					
    				</ul>
                @endif

				<ul class="nav navbar-nav navbar-right">
					@if(auth()->guest())
						@if(!Request::is('auth/login'))
							<li><a href="{{ url('/auth/login') }}">Login</a></li>
						@endif
						@if(!Request::is('auth/register'))
							<li><a href="{{ url('/auth/register') }}">Register</a></li>
						@endif
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ auth()->user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    @yield('bottom-assets');

</body>
</html>
