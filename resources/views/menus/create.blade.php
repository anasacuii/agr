@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Adauga un Meniu</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/save_menu') }}">
						{!! csrf_field() !!}

						<div class="form-group">
							<label class="col-md-4 control-label">Nume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Pret</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="price" value="{{ old('price') }}">
							</div>
						</div>

						

						<div class="form-group">
							<label class="col-md-4 control-label">Descriere</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="description" value="{{ old('description') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Tip</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="type" value="{{ old('type') }}" placeholder="alcol, dejun, ciorba, desert, diabetic, garnituri, gluten, nonalcol, paste, pizza, principal, raw, salata, snack or vegan">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
								    Adaugă Meniu
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
