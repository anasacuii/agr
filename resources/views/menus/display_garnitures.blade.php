@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-heading">Garnituri</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Nume</th>
   					<th>Pret</th>
   					<th>Descriere</th>
  				</tr>
  				@foreach($menus as $menu)
  				@if($menu->type == 'garnituri')
  				<tr>
					<td>{{$menu->name}}</td>
					<td>{{$menu->price}}</td>
					<td>{{$menu->description}}</td>			
					
				</tr>
				@endif
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager']))
    			    <a class="btn btn-primary" href="{{ url('/create_menu') }}">Adaugă Meniu</a>
    			   
                @endif
                </div>
				</div>
			</div>			

		</div>
	</div>
</div>
@endsection