@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Pranz-Cina</div>
				<div class="panel-body">
		<div class="tabel">
   			<div class="img"><a href="{{ url('/display_ciorbes_menus') }}"><img src="ciorba.jpg"></a>
  				<div class="desc">Ciorba</div>
			</div>
			<div class="img"><a href="{{ url('/display_principals_menus') }}"><img src="principal.jpg"></a>
  				<div class="desc">Fel Principal</div>
			</div>
			<div class="img"><a href="{{ url('/display_pastes_menus') }}"><img src="paste.jpg"></a>
  				<div class="desc">Paste</div>
			</div>
			<div class="img"><a href="{{ url('/display_salads_menus') }}"><img src="salad.jpg"></a>
  				<div class="desc">Salate</div>
			</div>
			<div class="img"><a href="{{ url('/display_garnitures_menus') }}"><img src="garnituri.jpg"></a>
  				<div class="desc">Garnituri</div>
			</div>
			
			</div>
		</div>
               

			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('bottom-assets')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/imagini.css') }}"/ >
@endsection