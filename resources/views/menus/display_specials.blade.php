@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Meniu Special</div>
				<div class="panel-body">
		<div class="tabel">
   			<div class="img"><a href="{{ url('/display_vegans_menus') }}"><img src="vegan.jpg"></a>
  				<div class="desc">Vegan</div>
			</div>
			<div class="img"><a href="{{ url('/display_raws_menus') }}"><img src="raw.jpg"></a>
  				<div class="desc">Raw-Vegan</div>
			</div>
			<div class="img"><a href="{{ url('/display_glutens_menus') }}"><img src="gluten.png"></a>
  				<div class="desc">Fara Gluten</div>
			</div>
			<div class="img"><a href="{{ url('/display_diabetics_menus') }}"><img src="diabetici.jpg"></a>
  				<div class="desc">Diabetici</div>
			</div>
			
			</div>
		</div>
                

			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('bottom-assets')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/imagini.css') }}"/ >
@endsection