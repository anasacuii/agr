@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Meniu</div>
				<div class="panel-body">
		<div class="tabel">
   			<div class="img"><a href="{{ url('/display_breakfasts_menus') }}"><img src="dejun.jpg"></a>
  				<div class="desc">Mic Dejun</div>
			</div>
			<div class="img"><a href="{{ url('/display_lunchs_menus') }}"><img src="lunch.jpg"></a>
  				<div class="desc">Pranz-cina</div>
			</div>
			<div class="img"><a href="{{ url('/display_deserts_menus') }}"><img src="desert.jpg"></a>
  				<div class="desc">Desert</div>
			</div>
			<div class="img"><a href="{{ url('/display_specials_menus') }}"><img src="special.jpg"></a>
  				<div class="desc">Meniu Special</div>
			</div>
			<div class="img"><a href="{{ url('/display_snacks_menus') }}"><img src="gustare_rece.jpg"></a>
  				<div class="desc">Gustari</div>
			</div>
			<div class="img"><a href="{{ url('/display_pizzas_menus') }}"><img src="pizza.jpg"></a>
  				<div class="desc">Pizza</div>
			</div>
			<div class="img"><a href="{{ url('/display_nonalcols_menus') }}"><img src="tea.jpg"></a>
  				<div class="desc">Bauturi Nonalcolice</div>
			</div>
			<div class="img"><a href="{{ url('/display_alcols_menus') }}"><img src="alcol.jpg"></a>
  				<div class="desc">Bauturi alcolice</div>
			</div>
		</div>
                

			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('bottom-assets')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/imagini.css') }}"/ >
@endsection