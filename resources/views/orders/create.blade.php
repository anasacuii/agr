@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Comenzi</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/save_order') }}">
						{!! csrf_field() !!}

						<div class="form-group">
							<label class="col-md-4 control-label">Masa</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="table_id" value="{{ old('table_id') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Descriere</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="description" value="{{ old('description') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Suma</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="sum" value="{{ old('sum') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Status</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="status" value="{{ old('status') }}" placeholder="normal, cancelled, refunded">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="datetimepicker"  name="date" value="{{ old('date') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Adauga comanda
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('bottom-assets')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.datetimepicker.css') }}"/ >
<script src="{{ asset('/js/jquery.datetimepicker.full.min.js') }}"></script>

<!--@DOC: DateTimePicker - http://xdsoft.net/jqplugins/datetimepicker/ -->
<script>
    window.onload = function(){
        jQuery.datetimepicker.setLocale('ro');
        jQuery('#datetimepicker').datetimepicker({
            format:'Y-m-d H:i:s'
        });
    }
</script>
@endsection