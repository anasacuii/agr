@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-heading">Comenzi</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Masa</th>
   					<th>Descriere</th>
   					<th>Suma</th>
   					<th>Status</th>
   					<th>Data</th>
  				</tr>
  				@foreach($orders as $order)
  				<tr>
					<td>{{$order->table_id}}</td>
					<td>{{$order->description}}</td>
					<td>{{$order->sum}}</td>
					<td>{{$order->status}}</td>
					<td>{{$order->date}}</td>					
				</tr>
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager' || 'ospatar']))
    			    <a class="btn btn-primary" href="{{ url('/create_order') }}">Adaugă comanda</a>
                @endif
                </div>
				</div>
			</div>			

		</div>
	</div>
</div>
@endsection
