@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Produse pentru diabetici</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Nume</th>
   					<th>Cantitate</th>
   					<th>Data aprovizionari</th>
   					<th>Modifica</th>
   					<th>Sterge</th>
  				</tr>
  				@foreach($products as $product)
  				@if($product->group == 'diabetics')
  				<tr>
					<td>{{$product->name}}</td>
					<td>{{$product->quantity}}</td>
					<td>{{$product->supply_date}}</td>
					<th><a href="#">Modifica<input type="hidden" value="$product->id"></a></th>
					<th><a href="#" value="$product->id"><input type="hidden" value="$product->id">Sterge</th>
					</tr>
				@endif
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager', 'bucatar', 'barman']))
    			    <a class="btn btn-primary" href="{{ url('/create_product') }}">Adaugă produs</a>
                @endif
                </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
