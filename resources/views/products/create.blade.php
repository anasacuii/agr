@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/save_product') }}">
						{!! csrf_field() !!}

						<div class="form-group">
							<label class="col-md-4 control-label">Nume</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Cantitate</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="quantity" value="{{ old('quantity') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Data aprovizionării</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="datetimepicker"  name="supply_date" value="{{ old('supply_date') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Tip</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="type" value="{{ old('type') }}" placeholder="fruits, vegetables, spices, grains, alcoholic, nonalcoholic or crockery">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Grup</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="group" value="{{ old('group') }}" placeholder="glutenfree, diabetics, vegan, rawvegan">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
								    Adaugă Produs
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('bottom-assets')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.datetimepicker.css') }}"/ >
<script src="{{ asset('/js/jquery.datetimepicker.full.min.js') }}"></script>

<!--@DOC: DateTimePicker - http://xdsoft.net/jqplugins/datetimepicker/ -->
<script>
    window.onload = function(){
        jQuery.datetimepicker.setLocale('ro');
        jQuery('#datetimepicker').datetimepicker({
            format:'Y-m-d H:i:s'
        });
    }
</script>
@endsection
