@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-heading">Retete</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Nume</th>
   					<th>Tip</th>
   					<th>Descriere</th>
   					<th>Modifica</th>
   					<th>Sterge</th>
  				</tr>
  				@foreach($recipes as $recipe)
  				<tr>
					<td>{{$recipe->name}}</td>
					<td>{{$recipe->type}}</td>
					<td>{{$recipe->description}}</td>					
					<th><a href="#">Modifica<input type="hidden" value="$recipe->id"></a></th>
					<th><a href="#" value="$reci[e->id"><input type="hidden" value="$recipe->id">Sterge</th>
				</tr>
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager' || 'ospatar']))
    			    <a class="btn btn-primary" href="{{ url('/create_order') }}">Adaugă reteta</a>
                @endif
                </div>
				</div>
			</div>			

		</div>
	</div>
</div>
@endsection
