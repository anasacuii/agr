@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Rezervari</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Masa</th>
   					<th>Data</th>
   					<th>Nume</th>
   					<th>Descriere</th>
   					<th>Status</th>
   					<th>Modifica</th>
   					<th>Sterge</th>
  				</tr>
  				@foreach($reservations as $reservation)
  				<tr>
					<td>{{$reservation->table_id}}</td>
					<td>{{$reservation->date}}</td>
					<td>{{$reservation->name}}</td>
					<td>{{$reservation->description}}</td>
					<td>{{$reservation->status}}</td>
					<th><a href="#">Modifica<input type="hidden" value="$reservation->id"></a></th>
					<th><a href="#" value="$reservation->id"><input type="hidden" value="$reservation->id">Sterge</th>
					</tr>
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager', 'ospatar']))
    			    <a class="btn btn-primary" href="{{ url('/create_reservation') }}">Adaugă rezervare</a>
                @endif
                </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
