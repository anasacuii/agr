@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-heading">Furnizori de echipamente</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Nume</th>
   					<th>Telefon</th>
   					<th>Email</th>
   					<th>Descriere</th>
   					<th>Adresa</th>
   					<th>Modifica</th>
   					<th>Sterge</th>
  				</tr>
  				@foreach($suppliers as $supplier)
  				@if($supplier->type == 'equipment')
  				<tr>
					<td>{{$supplier->name}}</td>
					<td>{{$supplier->phone_number}}</td>
					<td>{{$supplier->email}}</td>
					<td>{{$supplier->description}}</td>
					<td>{{$supplier->address}}</td>					
					<th><a href="#">Modifica<input type="hidden" value="$supplier->id"></a></th>
					<th><a href="#" value="$supplier->id"><input type="hidden" value="$supplier->id">Sterge</th>
				</tr>
				@endif
				@endforeach
				</table>
				</div>
				<div class="col-md-6 col-md-offset-4">
				@if(auth()->user()->hasRole(['manager']))
    			    <a class="btn btn-primary" href="{{ url('/create_supplier') }}">Adaugă furnizor</a>
                @endif
                </div>
				</div>
			</div>			

		</div>
	</div>
</div>
@endsection
