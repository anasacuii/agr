@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-heading">Utilizatori</div>
				<div class="panel-body">

                
                <div>
                <table id="tabel" style="width:100%">
                <tr>
    				<th>Nume</th>
  				</tr>
  				@foreach($users as $user)
  				<tr>
					<td>{{$user->name}}</td>					
				</tr>
				@endforeach
				</table>
				</div>
				</div>
			</div>			

		</div>
	</div>
</div>
@endsection
